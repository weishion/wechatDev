package action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.dom4j.DocumentException;

import util.CheckUtil;
import util.MessageUtil;
import weixinpojo.Image;
import weixinpojo.ImagetMessage;
import weixinpojo.ItemOfNewsMessage;
import weixinpojo.Location;
import weixinpojo.NewsMessage;
import weixinpojo.TextMessage;

import com.opensymphony.xwork2.ActionSupport;

import comm.MediaIdAndPicURL;
import comm.MessageType;



public class WeixinAction extends ActionSupport {
	private String signature;
	private String timestamp;
	private String nonce;
	private String echostr;

	/**发送普通信息测试
	 * @throws Exception
	 */
/*	
   public void execute2() throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		// 设置编码utf-8否则乱码
		response.setCharacterEncoding("utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		 //测试url微信连接开始 
		  System.out.println(signature);
		 
		  System.out.println(timestamp); 
		  System.out.println(nonce);
		  System.out.println(echostr);
		  System.out.println("1111"); 
		  if (CheckUtil.checkSignature(signature, timestamp, nonce)) {
		  		System.out.println("22222");
		   		System.out.println(echostr);
		  		out.print(echostr); 
		   } 
		  	System.out.println("333");
		// 测试url微信连接 结束
		 
		try {
			System.out.println("进入execute2");
			Map<String, String> map = MessageUtil.xmlToMap(request);
			String fromUserName = map.get("FromUserName");
			String toUserName = map.get("ToUserName");
			String msgType = map.get("MsgType");
			String content = map.get("Content");
			String message = "";
			String reply1 = "这是介绍1：微信公众平台在线客服 微信公众平台接口测试帐号申请 无需公众帐号、快速申请接口测试号 直接体验和测试公众平台所有高级接口关于腾讯 服务协议 客服中心 在线客服";
			String reply2 = "这是介绍2：此工具旨在帮助开发者检测调用【微信公众平台开发者API】时发送的请求参数是否正确,提交相关信息后可获得服务器的验证结果 ";
			String reply3 = "这是介绍3";
			if ("text".equals(msgType)) {
				TextMessage text = new TextMessage();
				text.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
				text.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
				text.setCreateTime(new Date().toString());
				text.setMsgType("text");
				// 对内容进行判断，设置自动回复
				if (content.equals("1")) {
					text.setContent(reply1);
				} else if (content.equals("2")) {
					text.setContent(reply2);
				} else if (content.equals("3")) {
					text.setContent(reply3);
				} else {
					text.setContent("您发送的消息是：" + content);
				}
				message = MessageUtil.textMessageToXml(text);
				// System.out.println(message);
			}

			out.print(message);
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}

	}*/
	
	
	/**关注事件测试
	 * @throws Exception
	 */
	public void execute2() throws Exception {
		//日志
		Logger logger = Logger.getLogger(WeixinAction.class);
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		// 设置编码utf-8,否则乱码
		response.setCharacterEncoding("utf-8");
		request.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		try {
			logger.info("进入execute2");
			
			//map接受request请求里面的所有参数
			Map<String, String> map = MessageUtil.xmlToMap(request);
			String fromUserName = map.get("FromUserName");
			String toUserName = map.get("ToUserName");
			String msgType = map.get("MsgType");
			//非空判断--必须
			String event = map.get("Event")==null?"":map.get("Event").toLowerCase();//微信发送过来的是大写
			String content = map.get("Content")==null ?"":map.get("Content");
			String eventKey =map.get("EventKey")==null?"":map.get("EventKey");
			//用来接受后面转换完的xml字符串
			String message = "";
			//回复信息
			String reply1 = "这是介绍1：微信公众平台在线客服 微信公众平台接口测试帐号申请 无需公众帐号、快速申请接口测试号 直接体验和测试公众平台所有高级接口关于腾讯 服务协议 客服中心 在线客服";
			String reply2 = "这是介绍2：此工具旨在帮助开发者检测调用【微信公众平台开发者API】时发送的请求参数是否正确,提交相关信息后可获得服务器的验证结果 ";
			String reply3 = "这是帮助介绍3";
			/**
			 * 进行msgType判断，是普通信息类型，还是事件类型
			 * */
			if ("text".equals(msgType)) {  //普通信息类型--text
				TextMessage text = new TextMessage();
				text.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
				text.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
				text.setCreateTime(new Date().getTime());
				text.setMsgType("text");
				// 对用户输入的内容进行判断，设置自动回复
				if (content.equals("1")) {
					text.setContent(reply1);
				} else if (content.equals("2")) {
					text.setContent(reply2);
				} else if (content.equals("帮助")) {
					text.setContent(reply3);
				} else {
					text.setContent("您发送的消息是：" + content);
				}
				message = MessageUtil.textMessageToXml(text);
				logger.info("message的内容======>"+message);
			}
			  /**
				*事件类型----并且是关注推事件
				*
				*/
			else if ("event".equals(msgType)&&"subscribe".equals(event)) {  //事件类型----并且是触发关注事件
				TextMessage text = new TextMessage();
				text.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
				text.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
				text.setCreateTime(new Date().getTime());
				text.setMsgType("text");
				text.setContent("欢迎关注：使用向导：试试回复“1”；回复“2”；回复“帮助”；有疑问请联系6666-6666666");
				message = MessageUtil.textMessageToXml(text);
				logger.info("message的内容======>"+message);
			}else if (MessageType.MESSAGE_EVENT.equals(msgType)&&MessageType.MESSAGE_LOCATION.equals(event)) {  //事件类型----并且是地理位置事件
				logger.info("地理位置======>");
				Location location = new Location();
				String latitude = (String) map.get("Latitude")==null ? "" :(String) map.get("Latitude");
				String longitude = (String) map.get("Longitude")==null ? "" :(String) map.get("Longitude");
				String precision = (String) map.get("Precision")==null ? "" :(String) map.get("Precision");
				location.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
				location.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
				location.setCreateTime(new Date().getTime());
				location.setLatitude(latitude);
				location.setLongitude(longitude);
				location.setPrecision(precision);
				message = MessageUtil.locationToXml(location);
				logger.info("backXml的内容======>"+message);
			}
			
			
			
				   /**
					*事件类型----并且是点击推事件
					*根据eventKey的值进行不同交互
					*
					*/
			 else if ("event".equals(msgType)&&"click".equals(event)){  
					if(eventKey.equals("tel")){
						TextMessage text = new TextMessage();
						text.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
						text.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
						text.setCreateTime(new Date().getTime());
						text.setMsgType("text");
						text.setContent("联系我们，电话：6666-6666666");
						message = MessageUtil.textMessageToXml(text);
						logger.info("message的内容======>"+message);
					}
					if(eventKey.equals("help")){
						//发送文本消息
					   TextMessage text = new TextMessage();
						text.setFromUserName(toUserName);// 服务器的发送方是微信的接收方
						text.setToUserName(fromUserName);// 服务器的接受方是微信的发送方
						text.setCreateTime(new Date().getTime());
						text.setMsgType("text");
						text.setContent("请回复“帮助”以获得帮助信息。");
						message = MessageUtil.textMessageToXml(text);
						logger.info("message的内容======>"+message);
					}
					if(eventKey.equals("pic")){   //发送图片消息
						//发送图片消息  
						ImagetMessage imagetMessage = new ImagetMessage();
						Image image = new Image();
						//Xa9Z4x2eQ7SelZ8fjfJ_H3q0c7DIpMfe9HUSNEUwGsf7xxZZTDqtj_J5ns2NbaEc
						image.setMediaId("Xa9Z4x2eQ7SelZ8fjfJ_H3q0c7DIpMfe9HUSNEUwGsf7xxZZTDqtj_J5ns2NbaEc");
						imagetMessage.setFromUserName(toUserName);
						imagetMessage.setToUserName(fromUserName);
						imagetMessage.setCreateTime(new Date().toString());
						imagetMessage.setMsgType("image");
						imagetMessage.setImage(image);
						message = MessageUtil.imageMessageToXml(imagetMessage);
						logger.info("message的内容======>"+message);
					}
					if(eventKey.equals("picMessge")){
						//发送图问消息  picMessge
						//发送图文消息  NewsMessge
						logger.info("发送图文");
						List<ItemOfNewsMessage> itemList = new ArrayList<ItemOfNewsMessage>();
						NewsMessage newsMessage = new NewsMessage();
						ItemOfNewsMessage item = new ItemOfNewsMessage();
						item.setTitle("标题");
						item.setDescription("图片描述");
						item.setPicUrl(MediaIdAndPicURL.PICURL1);
						item.setUrl("http://www.baidu.com");
						ItemOfNewsMessage item2 = new ItemOfNewsMessage();
						item2.setTitle("标题2");
						item2.setDescription("图片描述2");
						item2.setPicUrl(MediaIdAndPicURL.PICURL3);
						item2.setUrl("http://www.sohu.com");
						//添加
						itemList.add(item);
						itemList.add(item2);
						newsMessage.setFromUserName(toUserName);
						newsMessage.setToUserName(fromUserName);
						newsMessage.setMsgType(MessageType.MESSAGE_NEWS);
						newsMessage.setCreateTime(new Date().getTime());
						newsMessage.setArticleCount(itemList.size());
						newsMessage.setArticles(itemList);
						message = MessageUtil.newsMessageToXml(newsMessage);
						logger.info("message的内容======>"+message);
						}
					
			}
			out.print(message);//一定要返回给微信服务器
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			out.close();//关闭资源
		}

	}

	
	
	
	//set 和  get
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getEchostr() {
		return echostr;
	}

	public void setEchostr(String echostr) {
		this.echostr = echostr;
	}

}
