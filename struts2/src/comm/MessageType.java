package comm;

/**微信消息类型,事件常量类
 * @author hwx
 * 
 *
 */
public class MessageType {
	
		public static final String MESSAGE_TEXT = "text";//文本消息
		public static final String MESSAGE_IMAGE = "image";//图片消息
		public static final String MESSAGE_VOICE = "voice";//语音
		public static final String MESSAGE_VIDEO = "video";
		public static final String MESSAGE_LINK = "link";
		public static final String MESSAGE_LOCATION = "location";
		public static final String MESSAGE_NEWS = "news";//图文消息
		public static final String MESSAGE_EVENT = "event";
		public static final String MESSAGE_SUBSCRIBE = "subscribe";
		public static final String MESSAGE_UNSUBSCRIBE = "unsubscribe";
		public static final String MESSAGE_CLICK = "click";
		public static final String MESSAGE_VIEW = "view";
}
