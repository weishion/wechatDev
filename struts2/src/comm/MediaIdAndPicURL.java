package comm;

/**
 * Image节点里面的MediaId常量类和图片url：picURL
 * 图片上传到微信服务器自动生成，并记录到此类里
 * @author hwx
 *
 */
public class MediaIdAndPicURL {
	/**
	 * MediaId
	 * */
	public static final String IMA1 = "Xa9Z4x2eQ7SelZ8fjfJ_H3q0c7DIpMfe9HUSNEUwGsf7xxZZTDqtj_J5ns2NbaEc";
	public static final String IMA2 = "ePMR8b2jQ1Xy95SboMn5UmsiE7I8oJfAKLrd8jzzgoVmklxIgajKGAvAtZZuBXts";
	public static final String IMA3 = "O6zBwRYP0R0FUrtk4aYi2EtPalAYDSIm6iBMQlGq19dzy2Vq7P9n-s3GdObqu_rS";
	
	
	/**
	 * PicURL
	 * */
	
	/** 
	 * @Fields PICURL1 : 外网映射地址+图片
	 */ 
	public static final String PICURL1 = "http://huangwx.ngrok.cc/WeChatPub/imgs/corporateCul.png";
	/** 
	 * @Fields PICURL2 : 百度找的图片地址
	 */ 
	public static final String PICURL2 = "http://image.baidu.com/search/detail?ct=503316480&z=undefined&tn=baiduimagedetail&ipn=d&word=%E5%9B%BE%E7%89%87&step_word=&ie=utf-8&in=&cl=2&lm=-1&st=undefined&cs=1794894692,1423685501&os=2269231183,2892498381&simid=3483244408,577623349&pn=0&rn=1&di=70852147080&ln=1982&fr=&fmq=1493081919020_R&fm=&ic=undefined&s=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&is=0,0&istype=0&ist=&jit=&bdtype=0&spn=0&pi=0&gsm=0&objurl=http%3A%2F%2Fpic6.huitu.com%2Fres%2F20130116%2F84481_20130116142820494200_1.jpg&rpstart=0&rpnum=0&adpicid=0";
	/** 
	 * @Fields PICURL3 : 网上找的图片地址1
	 */ 
	public static final String PICURL3 = "http://365jia.cn/uploads/13/0301/5130c2ff93618.jpg";
	
	/** 
	 * @Fields PICURL4 :  网上找的图片地址2
	 */ 
	public static final String PICURL4 = "http://discuz.comli.com/weixin/weather/icon/cartoon.jpg";
	/** 
	 * @Fields PICURL5 : 项目服务器地址+图片
	 */ 
	public static final String PICURL5 = "http://112.74.234.80/WeChatPub/imgs/corporateCul.png";
	/** 
	 * @Fields PICURL6 : 网上找的图片地址3
	 */ 
	public static final String PICURL6 = "http://e.hiphotos.bdimg.com/wisegame/pic/item/9e1f4134970a304e1e398c62d1c8a786c9175c0a.jpg";
	
	public static final String PICURL7 = "http://huangwx.ngrok.cc/WeChatPub/3.jpg";

}
