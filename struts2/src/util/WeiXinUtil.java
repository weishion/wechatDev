package util;

import httptcp.HttpRequest;

import org.apache.log4j.Logger;

import weixinpojo.AccessToken;


import net.sf.json.JSONObject;

/** 
* @ClassName: WeiXinUtil 
* @Description:微信相关工具类，可以 获取accessToken，和其他需要的接口参数
* @author  
* @date 2017-4-26 下午2:32:41 
* 
*/
public class WeiXinUtil {
	
	public static final String APPID = "wx6f9363e6186ca86c";
	public static final  String APPSECRET = "fe325a0677aa75236a14125409561a8e";
	private static Logger logger = Logger.getLogger(WeiXinUtil.class);
		/** 
		* @Title: getAccessToken 
		* @Description: 以get形式获取accesstoken
		* @return String    
		* @throws 
		*/
		public static String  getAccessToken(){
			logger.info("获取accesstoken开始");
			String URL = "https://api.weixin.qq.com/cgi-bin/token";
			//https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
			String param = "grant_type=client_credential&appid="+APPID+"&secret="+APPSECRET;
			String rs = HttpRequest.sendGet(URL, param);
			JSONObject json=JSONObject.fromObject(rs);  //字符串转json
		    if(json.containsKey("access_token")){  
		    		AccessToken accessToken = (AccessToken) JSONObject.toBean(json, AccessToken.class);//json转对象
		    		logger.info("获取accesstoken成功");
		    		return accessToken.getAccess_token();  
		    }
		    else{
		    	   logger.info("获取accesstoken失败");
		    	   return "error";
		    }  
		}
		public static void main(String[] args) {
			logger.info(getAccessToken());
		}
		
		
		
}	
