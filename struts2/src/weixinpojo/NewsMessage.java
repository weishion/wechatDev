package weixinpojo;

import java.util.List;

/**
 * 图文消息类
 *@author hwx
 */
public class NewsMessage  extends BasePo{
	
	private int ArticleCount;
	private List<ItemOfNewsMessage> Articles  ;
	
	
	public int getArticleCount() {
		return ArticleCount;
	}
	public void setArticleCount(int articleCount) {
		ArticleCount = articleCount;
	}
	public List<ItemOfNewsMessage> getArticles() {
		return Articles;
	}
	public void setArticles(List<ItemOfNewsMessage> articles) {
		Articles = articles;
	}
	
	
	
	
}
